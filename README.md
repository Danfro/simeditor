# SimEditor

... an app for Ubuntu Touch to display some basic sim card information. It does also allow to add and edit the phone number stored on the sim card. This number will then be visible in address-book app and system settings app.

This apps purpose is to provide a temporary option for users until those functions are implemented into system settings. There is more testing needed for those functions to be implemented. This too can be achieved with this app.

*Note: Not all providers do allow phone number editing. If editing does not work at all, this is not a bug.*

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/simeditor.danfro)

## Credits

Lionel Duboeuf (@lduboeuf) had the idea for this app. The original coding was done by him. I did first testing and added some improvents to the UI. Now I am "hosting" this app and maintain the code.

## This app runs unconfined!

In order to write information on the sim card the app needs to run unconfined.

## Screenshot

<img src="screenshot.png" alt="screenshot" width="220"/>
