/*
 * Copyright (C) 2020  Lionel Duboeuf and Daniel Frost
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Telephony.PhoneNumber 0.1 as PhoneNumber
import Lomiri.Telephony 0.1

import MeeGo.QOfono 0.2

Item {

    id: root
    property int simNb: 0

    property string path

    height: simInfoList.height
    width: parent.width

    function getCountryCode() {
            var localeName = Qt.locale().name
            return localeName.substr(localeName.length - 2, 2)
    }

    PhoneUtils {
        id: phoneUtils
    }

    OfonoSimManager {
        id: simMng
        modemPath: path
    }

    Column {
        id: simInfoList
        width: parent.width

        // see for info: https://github.com/sailfishos/libqofono/blob/master/plugin/plugins.qmltypes

        SimItem {
            color: LomiriColors.blue
            title.text: i18n.tr("SIM #%1").arg(simNb+1)
            title.color: theme.palette.normal.raised //always white to provide better contrast on blue background
        }

        SimItem {
            title.text: simMng.subscriberNumbers[0] === undefined ? i18n.tr("empty") : simMng.subscriberNumbers[0]
            subtitle.text: i18n.tr("SubscriberNumber")
            Button {
                anchors.right: parent.right
                anchors.rightMargin: units.gu(2)
                anchors.verticalCenter: parent.verticalCenter
                text: i18n.tr("change number")
                onClicked: PopupUtils.open(dialog)
            }
        }
        //   Q_PROPERTY(QString subscriberIdentity READ subscriberIdentity NOTIFY subscriberIdentityChanged)
        SimItem {
            title.text: simMng.subscriberIdentity === "" ? i18n.tr("empty") : simMng.subscriberIdentity
            subtitle.text: i18n.tr("SubscriberIdentity (IMSI)")
        }
        //   Q_PROPERTY(QString mobileCountryCode READ mobileCountryCode NOTIFY mobileCountryCodeChanged)
        SimItem {
            title.text: simMng.mobileCountryCode === "" ? i18n.tr("empty") : simMng.mobileCountryCode
            subtitle.text: i18n.tr("MobileCountryCode (MCC)")
        }
        //   Q_PROPERTY(QStringList preferredLanguages READ preferredLanguages NOTIFY preferredLanguagesChanged)
        SimItem {
            title.text: simMng.preferredLanguages.length === 0 ? i18n.tr("empty") : simMng.preferredLanguages.toString()
            subtitle.text: i18n.tr("Preferred languages")
        }
        //   Q_PROPERTY(QString mobileNetworkCode READ mobileNetworkCode NOTIFY mobileNetworkCodeChanged)
        SimItem {
            title.text: simMng.mobileNetworkCode === "" ? i18n.tr("empty") : simMng.mobileNetworkCode
            subtitle.text: i18n.tr("MobileNetworkCode (MNC)")
        }
        //   Q_PROPERTY(QString cardIdentifier READ cardIdentifier NOTIFY cardIdentifierChanged)
        SimItem {
            title.text: simMng.cardIdentifier === "" ? i18n.tr("empty") : simMng.cardIdentifier
            subtitle.text: i18n.tr("CardIdentifier (ICCID)")
        }
        //   Q_PROPERTY(QString serviceProviderName READ serviceProviderName NOTIFY serviceProviderNameChanged)
        SimItem {
            title.text: simMng.serviceProviderName === "" ? i18n.tr("empty") : simMng.serviceProviderName.toString()
            subtitle.text: i18n.tr("Service provider name")
        }
        //   Q_PROPERTY(QStringList subscriberNumbers READ subscriberNumbers WRITE setSubscriberNumbers NOTIFY subscriberNumbersChanged)
        SimItem {
            title.text: simMng.subscriberNumbers.length === 0 ? i18n.tr("empty") : simMng.subscriberNumbers.toString()
            subtitle.text: i18n.tr("Subscriber numbers")
        }
        //   Q_PROPERTY(QVariantList lockedPins READ lockedPins NOTIFY lockedPinsChanged)
        SimItem {
            title.text: simMng.lockedPins.length === 0 ? i18n.tr("empty") : simMng.lockedPins.toString()
            subtitle.text: i18n.tr("Locked pins")
        }

        // not working due to unknown QVariantMap
        //   Q_PROPERTY(QVariantMap serviceNumbers READ serviceNumbers NOTIFY serviceNumbersChanged)
        // SimItem {
        //     title.text: simMng.serviceNumbers === undefined ? i18n.tr("empty") : simMng.serviceNumbers  //QVariantMap
        //     subtitle.text: i18n.tr("Service numbers")
        // }
        //   Q_PROPERTY(PinType pinRequired READ pinRequired NOTIFY pinRequiredChanged)
        // Enum {
        //     name: "PinType"
        //     values: {
        //         "NoPin": 0,
        //         "SimPin": 1,
        //         "SimPin2": 2,
        //         "PhoneToSimPin": 3,
        //         "PhoneToFirstSimPin": 4,
        //         "NetworkPersonalizationPin": 5,
        //         "NetworkSubsetPersonalizationPin": 6,
        //         "ServiceProviderPersonalizationPin": 7,
        //         "CorporatePersonalizationPin": 8,
        //         "SimPuk": 9,
        //         "SimPuk2": 10,
        //         "PhoneToFirstSimPuk": 11,
        //         "NetworkPersonalizationPuk": 12,
        //         "NetworkSubsetPersonalizationPuk": 13,
        //         "CorporatePersonalizationPuk": 14
        //     }
        // }
        // SimItem {
        //     title.text: simMng.pinRequired === undefined ? i18n.tr("empty") : simMng.pinRequired
        //     subtitle.text: i18n.tr("Pin required")
        //     Component.onCompleted: console.log(simMng.pinRequired)
        // }
        // not working due to unknown QVariantMap
        //   Q_PROPERTY(QVariantMap pinRetries READ pinRetries NOTIFY pinRetriesChanged)
        // SimItem {
        //     title.text: simMng.pinRetries === undefined ? i18n.tr("empty") : simMng.pinRetries  //QVariantMap
        //     subtitle.text: i18n.tr("Pin retries")
        // }
        //   Q_PROPERTY(bool fixedDialing READ fixedDialing NOTIFY fixedDialingChanged)
        // SimItem {
        //     title.text: simMng.fixedDialing === undefined ? i18n.tr("empty") : simMng.fixedDialing
        //     subtitle.text: i18n.tr("Fixed dialing")
        // }
        //   Q_PROPERTY(bool barredDialing READ barredDialing NOTIFY barredDialingChanged)
        // SimItem {
        //     title.text: simMng.barredDialing === undefined ? i18n.tr("empty") : simMng.barredDialing
        //     subtitle.text: i18n.tr("Barred dialing")
        // }
        //   Q_PROPERTY(bool present READ present NOTIFY presenceChanged)
        // SimItem {
        //     title.text: simMng.present === undefined ? i18n.tr("empty") : simMng.present
        //     subtitle.text: i18n.tr("Present")
        // }
    }

    Component {
        id: dialog
        Dialog {
            id: dialogue
            title: i18n.tr("SubscriberNumber Change")
            text: i18n.tr("Insert your international phone number")
            property bool secondConfirmation: false
            Label {
                id: feedbackText
                color: "red"
                visible: text.length > 0
            }

            PhoneNumber.PhoneNumberField {
                id: newNumber
                text: simMng.subscriberNumbers[0].length > 0 ? simMng.subscriberNumbers[0]: undefined
                autoFormat: true
                placeholderText: "+33 6 00 00 00 00"
                defaultRegion: getCountryCode()
                updateOnlyWhenFocused: false
                inputMethodHints: Qt.ImhDialableCharactersOnly

            }
            Row {
                id: row
                width: parent.width
                spacing: units.gu(1)
                Button {
                    width: parent.width/2 - row.spacing/2
                    text: i18n.tr("Cancel")
                    onClicked: PopupUtils.close(dialogue)
                }
                Button {
                    width: parent.width/2 - row.spacing/2
                    text: i18n.tr("Confirm")
                    color: LomiriColors.green
                    onClicked: {

                       phoneUtils.setCountryCode(getCountryCode())
                       var valid = phoneUtils.isPhoneNumber(newNumber.text) && newNumber.text[0] === "+"
                       if (!valid) {
                           feedbackText.text = i18n.tr("Wrong number format")
                       } else {

                           if (secondConfirmation) {
                               simMng.subscriberNumbers = [phoneUtils.normalizePhoneNumber(newNumber.text)]
                               PopupUtils.close(dialogue)
                           } else {
                               secondConfirmation = true
                               dialogue.text=""
                               newNumber.enabled = false
                               feedbackText.text = i18n.tr("Your phonenumber seems to be OK, confirm again to write it")
                               feedbackText.wrapMode = Text.WordWrap
                               feedbackText.color = "green"
                           }
                       }
                    }
                }
            }
        }
    }
}
