/*
 * Copyright (C) 2020  Lionel Duboeuf and Daniel Frost
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3

Page {
    id: aboutPage

    header: PageHeader {
        id: header
        title: i18n.tr("About")
    }

    //turn off firstrun wizard
    Component.onCompleted: settings.firstrun = false;

    Flickable {
        id: aboutFlickable
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded

        anchors {
            topMargin: aboutPage.header.height + units.gu(1)
            fill: parent
        }

        contentHeight: aboutColumn.childrenRect.height

        Column {
            id: aboutColumn

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            Item {
                id: appIcon
                width: parent.width
                height: app_icon.height + units.gu(4)

                LomiriShape {
                    id: app_icon

                    width: Math.min(aboutPage.width/5, 256)
                    height: width
                    anchors.centerIn: parent

                    source: Image {
                        id: icon_image
                        source: Qt.resolvedUrl("../assets/logo.svg")
                    }
                    radius: "medium"
                    aspect: LomiriShape.DropShadow
                }
            }

            ListItem {
                id: appLabel
                height: appLayout.height
                divider { visible: false; }
                ListItemLayout {
                    id: appLayout
                    title.text: i18n.tr("SimEditor")
                    title.font.pixelSize: units.gu(2.5)
                    title.horizontalAlignment: Text.AlignHCenter
                    subtitle.text: "v%1".arg(Qt.application.version)
                    subtitle.font.pixelSize: units.gu(1.75)
                    subtitle.horizontalAlignment: Text.AlignHCenter
                }
            }

            Repeater {
                id: listViewAbout

                model: [
                { name: i18n.tr("Get the source"), url: "https://gitlab.com/danfro/simreader/" },
                { name: i18n.tr("Report issues"),  url: "https://gitlab.com/danfro/simreader/issues" },
                { name: i18n.tr("Help translate"), url: "https://gitlab.com/danfro/simreader/" }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    height: layoutAbout.height
                    ListItemLayout {
                        id: layoutAbout
                        title.text : modelData.name
                        ProgressionSlot { }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }

            Spacer {}

            Label {
                id: headlineLabelInfo
                text: i18n.tr("About this app")
                font.pixelSize: units.gu(1.8)
                font.bold: true
                font.underline: true
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                }
            }

            Spacer {}

            Column {
                id: descriptionColumn
                spacing: units.gu(1)
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                    right: parent.right
                    rightMargin: units.gu(2)
                }

                Label {
                    text: i18n.tr("This app does read and display some basic information about your sim cards.")
                    wrapMode: Text.WordWrap
                    width: parent.width
                }

                Label {
                    text: i18n.tr("Some providers do not write the phone number on the sim card. This app does also allow to edit your phone numbers. Those will then be displayed in system settings and addressbook app.")
                    wrapMode: Text.WordWrap
                    width: parent.width
                }

                Label {
                    text: i18n.tr("Not all providers do allow number editing. If you can not edit your number at all, this is not a bug.")
                    color: theme.palette.normal.negative
                    wrapMode: Text.WordWrap
                    width: parent.width
                }

                Label {
                    text: i18n.tr("This app needs to run unconfined in order to edit sim card numbers. This apps functions will be integrated into system settings in the future. Until that is done this apps purpose is to gather feedback and do more testing.")
                    wrapMode: Text.WordWrap
                    width: parent.width
                }
            }

            Spacer {}

            Label {
                id: headlineLabelCreadits
                text: i18n.tr("Credits")
                font.pixelSize: units.gu(1.8)
                font.bold: true
                font.underline: true
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                }
            }

            Repeater {
                id: listViewCredits

                model: [
                    {
                        name: i18n.tr("Initial idea and coding by") + " Lionel Duboeuf",
                        summary: ""
                        // url: "https://???/"
                    },
                    {
                        name: i18n.tr("UI improvent and testing by") + " Daniel Frost",
                        summary: ""
                        // url: "https://???/"
                    },
                    {
                        name: i18n.tr("Maintainer") + " Daniel Frost",
                        summary: ""
                        // url: "https://???/"
                    },
                    {
                        name: i18n.tr("Translations:"),
                        summary: "Daniel Frost, Heimen Stoffels"
                        // url: "https://???/"
                    }
                ]

                delegate: ListItem {
                    divider { visible: false; }
                    height: layoutCredits.height
                    ListItemLayout {
                        id: layoutCredits
                        title.text : modelData.name
                        // subtitle.text : modelData.licence
                        summary.text: modelData.summary
                        summary.wrapMode: Text.WordWrap
                        // ProgressionSlot { }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }
        }
    }
}
