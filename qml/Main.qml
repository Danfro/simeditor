/*
 * Copyright (C) 2020  Lionel Duboeuf and Daniel Frost
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import MeeGo.QOfono 0.2

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'simeditor.danfro'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property var modems :[]
    property bool firstRun: true

    Settings {
        id: settings
        property bool firstrun: root.firstRun
    }

    PageStack {
        id: pageStack
        Component.onCompleted: push(Qt.resolvedUrl("SimData.qml"));
    }

    OfonoManager {
        id: ofonoManager
        onModemsChanged: {
            root.modems = modems.slice(0).sort()
            // console.log("ofonoManager",modems.slice(0).sort() )
            // console.log("path1: " + root.modems[0] )
            // console.log("path2: " + root.modems[1] )
        }
    }
}
