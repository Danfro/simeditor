/*
 * Copyright (C) 2020  Lionel Duboeuf and Daniel Frost
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3

Page {
    anchors.fill: parent

    header: PageHeader {
        id: pageHeader
        title: i18n.tr('SimEditor')

        trailingActionBar {
            actions: [
                Action {
                    iconName: "help-contents"
                    onTriggered: pageStack.push(Qt.resolvedUrl("About.qml"))
                }
            ]
        }
    }

    Component.onCompleted: firstRunTimer.start()

    Timer {
        id: firstRunTimer
        interval: 100
        running: false
        repeat: false
        onTriggered: {
            // show about page on first app start
            if (settings.firstrun === true) {
                pageStack.push(Qt.resolvedUrl("About.qml"));
             }
        }
    }

    Flickable {
        contentHeight: simColumn.childrenRect.height
        anchors {
            topMargin: pageHeader.height
            fill: parent
        }
        clip: true
        flickableDirection: Flickable.AutoFlickIfNeeded

        Column {
        id: simColumn

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            ListItem {
                width: parent.width
                height: simOne.height
                divider { visible: false; }

                Sim {
                    id: simOne
                    simNb: 0
                    path: root.modems[0] !== undefined ? root.modems[0] : ""

                }
            }

            ListItem {
                width: parent.width
                height: simTwo.height
                divider { visible: false; }
                visible: root.modems[1] !== undefined ? true : false

                Sim {
                    id: simTwo
                    simNb: 1
                    path: root.modems[1] !== undefined ? root.modems[1] : ""

                }
            }
        }
    }
}
